let userName;
let userNumber;
do {
    userName = prompt("Enter your name");
} while (userName === "" || !isNaN(userName));

do {
    userNumber = prompt("Enter your age");
} while (userNumber === "" || isNaN(userNumber));
if (userNumber < 18) {
    alert("Your not allowed to visit this website");
} else if (userNumber >= 18 && userNumber <= 22) {
    if (confirm("Do you want to continue") === false) {
        alert("Your not allowed to visit this website");
    } else {
        alert(`Welcome ${userName}`);
    }
} else {
    alert(`Welcome ${userName}`);
}